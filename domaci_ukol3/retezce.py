# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'
tri_slova= string1[:14]
append1 = []
append1.append ('...')
tri_slova.join(append1)
print (tri_slova, "...")
# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'
posledne = string1[-11:]
print (posledne)

# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'

mylist = string2.split(': ')
print (mylist)
# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.
slovo = string2
a=0
b=0
for pismeno in slovo:
    if pismeno in "a":
        a = a +1
    elif pismeno in slovo:
        if pismeno in "b":
            b = b+1
print ("písmeno a:", a)
print ("písmeno b:", b)


# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved:
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'


# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'

war=correct_string3[-3:], ":9"
print (war)

concerned=correct_string3[-25:-16], ":6"
print (concerned)

itself=correct_string3 [-15:-9], ":7"
print (itself)

wit=correct_string3[-8:-4], ":8"
print (wit)

which=correct_string3[22:27], ":5"
print (which)

ministry=correct_string3[4:12], ":2"
print (ministry)

the=correct_string3[0:3], ":1"
print (the)

of=correct_string3[13:15], ":3"
print (of)

peace=correct_string3[16:21], ":4"
print(peace)

spravne=(the, ministry, of, peace, which, concerned, itself, wit, war)
print (spravne)


