fridge = []


def add_food (food):
    if food ["expired"]== False:
        fridge.append(food)
        return True
    return False

def add_foods(*args):

    """
    Funkce prida do lednice vice potravin (pouze kdyz nejsou prosle), ktere budou zadany jako jednotlive parametry funkce
    :param (dicts) args: vice potravin zadanych jako jednotlive parametry
    :return: vrati True pokud se vsechny potraviny podari pridat. Pokud ne, tak vrati False
    """
    for food in args:
        if food ["expired"]== True:
            return False
    for food in args:
        if food ["expired"]== False:
            fridge.append(food)

    return True



def eat_food(name):
    for food in fridge:
        if food["name"] == name:
            fridge.remove(food)
            return True
    return False


def get_price():
    sum = 0
    for food in fridge:
        sum += food ['price']
    return int(sum)

def get_count():
    counter = 0
    for food in fridge:
        counter += 1
    return int(counter)


def get_average_price():
    return get_price()/get_count()


def remove_expired():
    expired_food = []
    for food in fridge:
        if food['expired']== True:
            expired_food.append(food)
    for food in expired_food:
        fridge.remove (food)
    return expired_food


def set_expired(name):
    for food in fridge:
        if food["name"] == name:
            food['expired'] = True
            return True
    return False


print('Pridani potraviny blue cheese:', add_food({'name': 'blue cheese', 'price': 40, 'expired': True}))
print('Pridani potraviny milk:', add_food({'name': 'milk', 'price': 15, 'expired': False}))

print('Pridani vice potravin:', add_foods({'name': 'ham', 'price': 30, 'expired': False},
                                           {'name': 'salad', 'price': 45, 'expired': False},
                                           {'name': 'egg', 'price': 4, 'expired': False},
                                           {'name': 'wine', 'price': 200, 'expired': False},
                                           {'name': 'orange juice', 'price': 40, 'expired': False}
                                           )
      )

print('Snedl jsem sunku:', eat_food('ham'))
print('Snedl jsem hovinko:', eat_food('poop'))

print('Celkova cena potravin:', get_price())
print('Celkovy pocet potravin:', get_count())
print('Prumerna cena potravin:', get_average_price())

print('Nastavuji expiraci vajicku:', set_expired('egg'))
print('Nastavuji expiraci vinu:', set_expired('wine'))
print('Nastavuji expiraci hovinku:', set_expired('poop'))

print('Odstranene potraviny:', remove_expired())



